from threading import Thread
import time
import threading
import random
import queue

# Longueur de la liste est fixée a 20
liste_size = 20
q = queue.Queue(liste_size)
verrou = threading.Lock()


# La fonction wait()
def wait():
    time.sleep(random.random())


class Producer(Thread):
    # Constructeur
    def __init__(self, q, i, count_producers):
        Thread.__init__(self)
        self.q = q
        self.i = i
        self.count_producers = count_producers

    # Fonction produce_item qui permet au producteur de génerer une valeurs entre 1 et 10
    # la valuer sera ajouter au dérnier élement de la liste (FIFO)
    def produce_item(self):
        item = random.randint(1, 10)
        self.q.put(item)
        return item

    def run(self):
        # Chaque producteur va ajouter 20 valeurs dans la liste.
        for itération in range(20):
            verrou.acquire()
            # Vérfier si la liste est pleine
            if not self.q.full():
                item = self.produce_item()
                wait()
                print('Producteur numéro [', str(self.i), '] a produit litem numéro : ', item, ' *** Liste : ',
                      str(list(self.q.queue)))
                verrou.release()
                wait()
        print("Producer Finished")


class Consumer(Thread):
    # Constructeur
    def __init__(self, q, i):
        Thread.__init__(self)
        self.q = q
        self.i = i

    def consum_item(self):
        item = self.q.get()
        return item

    def run(self):
        done = False
        while not done:
            verrou.acquire()
            # Vérfier si la liste est vide
            if not self.q.empty():
                item = self.consum_item()
                wait()
                # Si le consommateur récupére l'item 'None' c'est à dire que les producteurs ont fini leur travail
                # le programme va alors s'arreter
                if item is None:
                    done = True
                    verrou.release()
                else:
                    print('Consomateur numéro [', str(self.i), '] a récupérer litem numéro : ', item,
                          ' *** Liste : ', str(list(self.q.queue)))
                    verrou.release()
                    wait()
            else:
                verrou.release()
                wait()


def main():
    count_producers = 2
    count_consumers = 4

    producers = []
    consumers = []

    for i in range(count_producers):
        prod = Producer(q, i, count_producers)
        producers.append(prod)

    for i in range(count_consumers):
        cons = Consumer(q, i)
        cons.daemon = True
        consumers.append(cons)

    for p in producers:
        p.start()
        wait()

    for c in consumers:
        c.start()
        wait()

    for p in producers:
        p.join()

    # Si un consommateur récupére l'item 'NONE', il va a son tour notifier les autres consommateurs
    # cela va eviter les problémes dans le cas ou il y a plusieurs consommateurs.
    for c in consumers:
        q.put(None)

    for c in consumers:
        c.join()

    print("Programme 'DONE'")


if __name__ == '__main__':
    main()