from threading import Thread, Condition
import time
import random

# Longueur de la liste est fixée a 20
Items = []
liste_size = 20
condition = Condition()


# La fonction wait()
def wait():
    time.sleep(random.random())


class Producer(Thread):
    # Constructeur
    def __init__(self, i, Items):
        Thread.__init__(self)
        self.i = i
        self.Items = Items

    # Fonction produce_item qui permet au producteur de génerer une valeurs entre 1 et 10
    # la valuer sera ajouter au dérnier élement de la liste (FIFO)
    def produce_item(self):
        item = random.randint(1, 10)
        self.Items.append(item)
        return item

    def run(self):
        # Chaque producteur va ajouter 20 valeurs dans la liste.
        for itération in range(20):
            condition.acquire()
            if len(Items) == liste_size:
                print("Liste pleine, producteur en attente")
                condition.wait()
                print("Espace libre dans la liste, le consommateur notifiera le producteur")
                condition.release()
            else:
                item = self.produce_item()
                wait()
                print('Producteur numéro [', str(self.i), '] a produit l\'item numéro : ', item, ' *** Liste : ',
                      str(list(self.Items)))
                # Le consommateur va alors notifier le producteur.
                condition.notify()
                condition.release()
                wait()
        print('Le producteur numéro : [', self.i, '] a fini son travail')


class Consumer(Thread):
    # Constructeur
    def __init__(self, i, Items):
        Thread.__init__(self)
        self.i = i
        self.Items = Items

    # Fonction consum_item qui permet au consommateur de consommer la premiére valeur de la liste
    # pour respecter la notion FIFO
    def consum_item(self):
        return self.Items.pop(0)

    def run(self):
        done = False
        while not done:
            condition.acquire()
            if not Items:
                print("La liste est vide, le consommateur en attente")
                condition.wait()
                print("Le producteur a ajouter une valeur dans la lise, et notifiera le consommateur")
                condition.release()
            else:
                item = self.consum_item()
                wait()
                # Si le consommateur récupére l'item 'None' c'est à dire que les producteurs ont fini leur travail
                # le programme va alors s'arreter
                if item is None:
                    done = True
                    condition.release()
                else:
                    print('Le consomateur numéro :  [', str(self.i), '] a récupérer l\'item numéro : ', item,
                          ' *** Liste : ', str(list(self.Items)))
                    # Le consommateur va alors notifier le producteur
                    condition.notify()
                    condition.release()
                    wait()


def main():
    count_producers = 4
    count_consumers = 2

    producers = []
    consumers = []

    for i in range(count_producers):
        prod = Producer(i, Items)
        producers.append(prod)

    for i in range(count_consumers):
        cons = Consumer(i, Items)
        cons.daemon = True
        consumers.append(cons)

    for p in producers:
        p.start()

    for c in consumers:
        c.start()

    for p in producers:
        p.join()

    # Si un consommateur récupére l'item 'NONE', il va a son tour notifier les autres consommateurs
    # cela va eviter les problémes dans le cas ou il y a plusieurs consommateurs.
    for c in consumers:
        Items.append(None)

    for c in consumers:
        c.join()

    print("Programme 'DONE'")


if __name__ == '__main__':
    main()